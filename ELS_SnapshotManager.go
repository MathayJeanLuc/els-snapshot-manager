package main

import (
    "fmt"
    "strconv"
    "os"
    "net/http"
    "errors"
    "log"
    "github.com/natefinch/lumberjack"
    "net"
    "io/ioutil"
    "encoding/json"
    "time"
    "strings"
    "github.com/gorilla/mux"
    "github.com/gorilla/sessions"
    "html/template"
    "gopkg.in/robfig/cron.v2"
    "github.com/gorilla/schema"
    "database/sql"
    _ "github.com/mattn/go-sqlite3"
)

// Templates
var errorTemplateString string = `{{- $host := .Host }}{{- $status := .Status }}{{- $statusText := .StatusText }}<!doctype html>
<html>
	<head>
		<title>ELS_SnaphotManager</title>
		<style>
			body {    
		    margin: 0 !important;
		    padding: 0 !important;
		  }
		  main {
		  	margin: 0px 10px;
		  }
			.topnav {
    		background-color: #333;
    		overflow: hidden;
    		height: 60px;
    		display: flex;    
    		align-items: center;
			}
			.topnav a {
		    float: left;
		    color: #f2f2f2;
		    text-align: center;
		    padding: 10px 16px;
		    text-decoration: none;
		    font-size: 17px;
			}
			.topnav a:hover {
			    background-color: #ddd;
			    color: black;
			}
			.topnav a.active {
			    background-color: white;
			    color: black;
			}
			.topnav div.brand {
				float: left;
				color: #f2f2f2;
				text-align: center;
				margin-right: 15px;
				padding: 14px 16px;
				text-decoration: none;
				font-size: 17px;
				border-radius: 5px 5px 5px;
				border: 1px solid white;
			}
		</style>
	</head>
  <body>
  	<nav class="topnav">
			<div class="brand">ELS Snapshot Manager</div>
			<a href="http://{{ $host }}">Home</a>
		</nav>
		<main>
    <h1>Oups! Error {{ $status }} : {{ $statusText }}</h1>
    <h3>Is your Elasticsearch node at {{ .Es_ip }} : {{ .Es_port }} up and ready ?</h3>
    </main>
  </body>
</html>`

var clusterTemplateString string = `{{- $host := .Host }}{{- $cluster := .Cluster.ClusterName }}<!doctype html>
<html>
	<head>
		<title>ELS_SnaphotManager</title>
		<style>
			body {    
		    margin: 0 !important;
		    padding: 0 !important;
		  }
		  main {
		  	margin: 0px 10px;
		  }
		  ul {
		  	list-style-type: none;
		  }
			.topnav {
    		background-color: #333;
    		overflow: hidden;
    		height: 60px;
    		display: flex;    
    		align-items: center;
			}
			.topnav a {
		    float: left;
		    color: #f2f2f2;
		    text-align: center;
		    padding: 10px 16px;
		    text-decoration: none;
		    font-size: 17px;
			}
			.topnav a:hover {
			    background-color: #ddd;
			    color: black;
			}
			.topnav a.active {
			    background-color: white;
			    color: black;
			}
			.topnav div.brand {
				float: left;
				color: #f2f2f2;
				text-align: center;
				margin-right: 15px;
				padding: 14px 16px;
				text-decoration: none;
				font-size: 17px;
				border-radius: 5px 5px 5px;
				border: 1px solid white;
			}
		</style>
	</head>
	<body>
		<nav class="topnav">
			<div class="brand">ELS Snapshot Manager</div>
			{{- range $key, $value := .Repositories }}
       <a href="http://{{ $host }}/{{ $cluster }}/{{ $key }}">{{ $key }}</a>
      {{- end }}
		</nav>
		<main>
		<h1>Cluster : {{ .Cluster.ClusterName }}</h1>
		<h4>Cluster infos</h4>
		<p>Cluster node : {{ .Cluster.Name }}</p>
		<p>Cluster uuid : {{ .Cluster.ClusterUuid }}</p>
		<p>Cluster version : 
		  <ul>
		    <li>Number : {{ .Cluster.Version.Number }}</li>
		    <li>Build hash : {{ .Cluster.Version.BuildHash }}</li>
		    <li>Build timestamp : {{ .Cluster.Version.BuildTimestamp }}</li>
				<li>Build snapshot : {{ .Cluster.Version.BuildSnapshot }}</li>
				<li>Lucene version : {{ .Cluster.Version.LuceneVersion }}</li>
		</ul>
    </p>
    <p>Tagline : {{ .Cluster.TagLine }}</p>	    
    {{- if not .Repositories }}
    <p>No repositories registered!</p>
    {{- else }}
    <h4>Registered repositories</h4>
    <p>
      <ul>
      {{- range $key, $value := .Repositories }}
        <li>{{ $key }}<br>
        {{- range $k, $v := $value.Settings }}
            <span>({{ $k }} : {{ $v }})</span><br>
        {{- end }}
        </li>
      {{- end }}
      </ul>
    </p>
  	{{- end }}
  </main>
  </body>
</html>`

var repositoryTemplateString string = `{{- $host := .Host }}{{- $cluster := .Cluster }}{{- $repository := .Repository }}<!doctype html>
<html>
	<head>
		<title>ELS_SnaphotManager</title>
		<style>
			body {    
		    margin: 0 !important;
		    padding: 0 !important;
		  }
		  main {
		  	margin: 0px 10px;
		  }
		  ul {
		  	list-style-type: none;
		  }
		  a {
		  	text-decoration: none;
		  }
			.topnav {
    		background-color: #333;
    		overflow: hidden;
    		height: 60px;
    		display: flex;    
    		align-items: center;
			}
			.topnav a {
		    float: left;
		    color: #f2f2f2;
		    text-align: center;
		    padding: 10px 16px;
		    font-size: 17px;
			}
			.topnav a:hover {
			    background-color: #ddd;
			    color: black;
			}
			.topnav a.active {
			    background-color: white;
			    color: black;
			}
			.topnav div.brand {
				float: left;
				color: #f2f2f2;
				text-align: center;
				margin-right: 15px;
				padding: 14px 16px;
				text-decoration: none;
				font-size: 17px;
				border-radius: 5px 5px 5px;
				border: 1px solid white;
			}
			.topnav div.action {
				margin-left: auto;
				margin-right: 15px
			}
			.topnav div.action input[type="submit"]{
				background: green;
				color: #fff;
				font-size: 15px;
				padding: 5px 10px;
				border: 1px solid white;
				border-radius: 20px;
				text-shadow:none;
				cursor:pointer;
			}
		</style>
	</head>
  <body>
  	<nav class="topnav">
			<div class="brand">ELS Snapshot Manager</div>
			<a href="http://{{ $host }}">Home</a>
			<a href="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/cron">Repository tasks</a>
			<div class="action">
				<form action="http://{{ $host }}/{{ $cluster }}/{{ $repository }}" method="post">
      		<input type="submit" value="Create snapshot"/>
    		</form>
			</div>
		</nav>
		<main>
    <h1>Repository : {{ $repository }}</h1>
		{{- if not .RepositorySnapshots }}
    <p>No snapshots registered!</p>
		{{- else }}
    <h4>Registered snapshots</h4>
    <p>
      <ul>
      {{- range $key, $value := .RepositorySnapshots.Snapshots }}
          <li><a href="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/{{ $value.Snapshot }}">{{ $value.Snapshot }}</a></li>
      {{- end}}
      </ul>
    </p>
		{{- end }}
	</main>
  </body>
</html>`

var snapshotTemplateString string = `{{- $host := .Host }}{{- $cluster := .Cluster }}{{- $repository := .Repository }}{{- $snapshot := .Snapshot }}<!doctype html>
<html>
	<head>
		<title>ELS_SnaphotManager</title>
		<style>
			body {    
		    margin: 0 !important;
		    padding: 0 !important;
		  }
		  main {
		  	margin: 0px 10px;
		  }
		  ul {
		  	list-style-type: none;
		  }
			.topnav {
    		background-color: #333;
    		overflow: hidden;
    		height: 60px;
    		display: flex;    
    		align-items: center;
			}
			.topnav a {
		    float: left;
		    color: #f2f2f2;
		    text-align: center;
		    padding: 10px 16px;
		    text-decoration: none;
		    font-size: 17px;
			}
			.topnav a:hover {
			    background-color: #ddd;
			    color: black;
			}
			.topnav a.active {
			    background-color: white;
			    color: black;
			}
			.topnav div.brand {
				float: left;
				color: #f2f2f2;
				text-align: center;
				margin-right: 15px;
				padding: 14px 16px;
				text-decoration: none;
				font-size: 17px;
				border-radius: 5px 5px 5px;
				border: 1px solid white;
			}
			.topnav div.action {
				margin-left: auto;
				margin-right: 15px
			}
			.topnav div.action input[type="submit"]{
				background: green;
				color: #fff;
				font-size: 15px;
				padding: 5px 10px;
				border: 1px solid white;
				border-radius: 20px;
				text-shadow:none;
				cursor:pointer;
			}
			.topnav div.action form {
				display: inline;
				margin-left: 15px;
			}
		</style>
	</head>
  <body>
  	<nav class="topnav">
			<div class="brand">ELS Snapshot Manager</div>
			<a href="http://{{ $host }}">Home</a>
			<a href="http://{{ $host }}/{{ $cluster }}/{{ $repository }}">Back</a>
			<div class="action">
				<form action="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/{{ $snapshot }}" method="post" onsubmit="return confirm('Are you sure you want to restore this snapshot?');">
					<input name="_method" type="hidden" value="restore" />
		      <input type="submit" value="Restore snapshot"/>
		    </form>
		    <form action="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/{{ $snapshot }}" method="post" onsubmit="return confirm('Are you sure you want to delete this snapshot?');">
		    	<input name="_method" type="hidden" value="delete" />
		      <input type="submit" value="Delete snapshot"/>
		    </form>
	    </div>
		</nav>
		<main>
    <h1>Snapshot : {{ $snapshot }}</h1>
    <h4>Snapshot infos</h4>
    <p>
	    <ul>
	    {{- range $value := .Snapshots.Snapshots }}
	      <li> Name : {{ $value.Snapshot }}</li>
	      <li> Version ID : {{ $value.VersionId }}</li>
	      <li> Version : {{ $value.Version }}</li>
	      <li> Indexes : {{ $value.Indices }}</li>
	      <li> State : {{ $value.State }}</li>
	      <li> Start time : {{ $value.StartTime }}</li>
	      <li> Start time in ms : {{ $value.StartTimeMs }}</li>
	      <li> End time : {{ $value.EndTime }}</li>
	      <li> End time in ms : {{ $value.EndTimeMs }}</li>
	      <li> Duration in ms : {{ $value.Duration }}</li>
	      {{- if $value.Failures }}
	      <li> Failures : 
	        <ul>
	        {{- range $val := $value.Failures}}
	        	<li>Index : {{ $val.Index }}, IndexUuid : {{ $val.IndexUuid }}, ShardId : {{ $val.ShardId }}, Reason : {{ $val.Reason }}, NodeId : {{ $val.NodeId }}, Status : {{ $val.Status }}</li>
	        {{- end }}
	        </ul>
	      {{- else }}
	      <li> Failures : []</li>
	      {{- end }}
	      <li> Shards : <br>
	          Total : {{ $value.Shards.Total }}<br>
	          Failed : {{ $value.Shards.Failed }}<br>
	          Successful : {{ $value.Shards.Successful }}<br>
	      </li>
	    {{- end }}
	    </ul>
    </p>    
  </main>
  </body>
</html>`

var getCronTemplateString string = `{{- $host := .Host }}{{- $cluster := .Cluster }}{{- $repository := .Repository }}{{- $crons := .Crons}}<!doctype html>
<html>
  <head>
  	<title>ELS_SnaphotManager</title>
    <style>
			body {    
		    margin: 0 !important;
		    padding: 0 !important;
		  }
		  main {
		  	margin: 0px 10px;
		  }
    .topnav {
			background-color: #333;
			overflow: hidden;			
  		height: 60px;
  		display: flex;    
  		align-items: center;
		}
		.topnav a {
	    float: left;
	    color: #f2f2f2;
	    text-align: center;
	    padding: 10px 16px;
	    text-decoration: none;
	    font-size: 17px;
		}
		.topnav a:hover {
		    background-color: #ddd;
		    color: black;
		}
		.topnav a.active {
		    background-color: white;
		    color: black;
		}
		.topnav div.brand {
			float: left;
			color: #f2f2f2;
			text-align: center;
			margin-right: 15px;
			padding: 14px 16px;
			text-decoration: none;
			font-size: 17px;
			border-radius: 5px 5px 5px;
			border: 1px solid white;
		}
		.topnav div.action {
				float: right;
				margin-left: auto;
				margin-right: 15px
		}
		.topnav div.action a {
			background: green;
			color: #fff;
			font-size: 17px;
			padding: 5px 10px;
			border: 1px solid white;
			border-radius: 20px;
			text-shadow:none;
		}
    table {
      border-collapse: collapse;
      width: 700px;
    }

    th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    td {
      border: 1px solid #dddddd;
      text-align: center;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
    </style>
  </head>
  <body>
  	<nav class="topnav">
			<div class="brand">ELS Snapshot Manager</div>
			<a href="http://{{ $host }}">Home</a>
			<a href="http://{{ $host }}/{{ $cluster }}/{{ $repository }}">Back</a>
			<div class="action">
				<a href="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/cron/new">Create task</a>
			</div>
		</nav>
		<main>
    <h1>{{ $repository }} tasks</h1>
    <table>
      <tr>
        <th>Cron name</th>
        <th>Seconds</th>
        <th>Minutes</th>
        <th>Hours</th>
        <th>Day of month</th>
        <th>Month</th>
        <th>Day of week</th>
      </tr>
      {{- if $crons }}
      {{- range $key,$val := $crons }}
      <tr>
        <td>{{ $val.Name }}</td>                
        <td>{{ $val.Seconds }}</td>
        <td>{{ $val.Minutes }}</td> 
        <td>{{ $val.Hours }}</td> 
        <td>{{ $val.DayOfMonth }}</td> 
        <td>{{ $val.Month }}</td> 
        <td>{{ $val.DayOfWeek }}</td> 
        <td><form method="post" onsubmit="return confirm('Are you sure you want to delete this cron?');" action="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/cron/{{ $val.Id }}"><input type="submit" value="Delete"></form></td>              
      </tr>
      {{- end }}
      {{- else }}
      <tr>
        <td colspan="7">No crons registered for this repository</td>
      </tr>
      {{- end }}
    </table>
  </main>
  </body>
</html>`

var newCronTemplateString string = `{{- $host := .Host }}{{- $cluster := .Cluster }}{{- $repository := .Repository }}{{- $cron := .Cron }}{{- $msg := .Msg }}<!doctype html>
<html>
  <head>
  	<title>ELS_SnaphotManager</title>
    <style>
			body {    
		    margin: 0 !important;
		    padding: 0 !important;
		  }
		  main {
		  	margin: 0px 10px;
		  }
    .topnav {
    		background-color: #333;
    		overflow: hidden;
    		height: 60px;
  			display: flex;    
  			align-items: center;
			}
			.topnav a {
		    float: left;
		    color: #f2f2f2;
		    text-align: center;
		    padding: 10px 16px;
		    text-decoration: none;
		    font-size: 17px;
			}
			.topnav a:hover {
			    background-color: #ddd;
			    color: black;
			}
			.topnav a.active {
			    background-color: white;
			    color: black;
			}
			.topnav div.brand {
				float: left;
				color: #f2f2f2;
				text-align: center;
				margin-right: 15px;
				padding: 14px 16px;
				text-decoration: none;
				font-size: 17px;
				border-radius: 5px 5px 5px;
				border: 1px solid white;
			}
    .alert { 
    	position: absolute;
    	background-color: white;
      color: red;
      border-radius: 5px 5px 5px;
			border: 1px solid red;
			padding: 10px 16px;
			text-align:center;
			left: 50%;
			margin-left: -120px;
			margin-top:0
    }
    table {
      border-collapse: collapse;
      width: 700px;
    }

    th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    td {
      border: 1px solid #dddddd;
      text-align: center;
      padding: 8px;
    }

    tr:nth-child(even) {
	    background-color: #dddddd;
    }
    </style>
  </head>
  <body>
  	<nav class="topnav">
			<div class="brand">ELS Snapshot Manager</div>
			<a href="http://{{ $host }}">Home</a>
			<a href="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/cron">Back</a>
			{{- if $msg }}
    <p class="alert">{{ $msg }}</p>
    {{- end}}						
		</nav>
		<main>
    <h1>{{ $repository }} tasks</h1>
    <h3>Cron creation</h3>    
    <table>
      <tr>
        <th>Cron name</th>
        <th>Seconds</th>
        <th>Minutes</th>
        <th>Hours</th>
        <th>Day of month</th>
        <th>Month</th>
        <th>Day of week</th>
      </tr>            
      <tr>
	      <form method="post" action="http://{{ $host }}/{{ $cluster }}/{{ $repository }}/cron/new">
          <td><input type="text" name="name" size="10"{{- if $cron }} value="{{ $cron.Name }}"{{- end }}></td>                
          <td><input type="text" name="seconds" size="2"{{- if $cron }} value="{{ $cron.Seconds }}"{{- end }}></td>
          <td><input type="text" name="minutes" size="2"{{- if $cron }} value="{{ $cron.Minutes }}"{{- end }}></td> 
          <td><input type="text" name="hours" size="2"{{- if $cron }} value="{{ $cron.Hours }}"{{- end }}></td> 
          <td><input type="text" name="dayOfMonth" size="2"{{- if $cron }} value="{{ $cron.DayOfMonth }}"{{- end }}></td> 
          <td><input type="text" name="month" size="2"{{- if $cron }} value="{{ $cron.Month }}"{{- end }}></td> 
          <td><input type="text" name="dayOfWeek" size="2"{{- if $cron }} value="{{ $cron.DayOfWeek }}"{{- end }}></td> 
          <td><input type="submit" value="Save"></td>
	      </form>
      </tr>            
    </table> 
    <h4>Cron Expression Format</h4>
		<p>
		A cron expression represents a set of times, using 6 space-separated fields.
		</p>
		<pre>
Field name   | Mandatory? | Allowed values  | Allowed special characters
----------   | ---------- | --------------  | --------------------------
Seconds      | Yes        | 0-59            | * / , -
Minutes      | Yes        | 0-59            | * / , -
Hours        | Yes        | 0-23            | * / , -
Day of month | Yes        | 1-31            | * / , - ?
Month        | Yes        | 1-12 or JAN-DEC | * / , -
Day of week  | Yes        | 0-6 or SUN-SAT  | * / , - ?
		</pre>
		<p>
		Note: Month and Day-of-week field values are case insensitive.  &#34;SUN&#34;, &#34;Sun&#34;,
		and &#34;sun&#34; are equally accepted.
		</p>
		<h4>Special Characters</h4>
		<p>
		Asterisk ( * )
		</p>
		<p>
		The asterisk indicates that the cron expression will match for all values of the
		field; e.g., using an asterisk in the 5th field (month) would indicate every
		month.
		</p>
		<p>
		Slash ( / )
		</p>
		<p>
		Slashes are used to describe increments of ranges. For example 3-59/15 in the
		1st field (minutes) would indicate the 3rd minute of the hour and every 15
		minutes thereafter. The form &#34;*\/...&#34; is equivalent to the form &#34;first-last/...&#34;,
		that is, an increment over the largest possible range of the field.  The form
		&#34;N/...&#34; is accepted as meaning &#34;N-MAX/...&#34;, that is, starting at N, use the
		increment until the end of that specific range.  It does not wrap around.
		</p>
		<p>
		Comma ( , )
		</p>
		<p>
		Commas are used to separate items of a list. For example, using &#34;MON,WED,FRI&#34; in
		the 5th field (day of week) would mean Mondays, Wednesdays and Fridays.
		</p>
		<p>
		Hyphen ( - )
		</p>
		<p>
		Hyphens are used to define ranges. For example, 9-17 would indicate every
		hour between 9am and 5pm inclusive.
		</p>
		<p>
		Question mark ( ? )
		</p>
		<p>
		Question mark may be used instead of &#39;*&#39; for leaving either day-of-month or
		day-of-week blank.
		</p> 
	</main>
  </body>
</html>`

// Constantes
const dbpath = "crons.db"

// Variables globales)
var es_ip_Ptr *string
var es_port_Ptr *string
var listen_port_Ptr *string
var snapshot_retention_ptr *int
var store = sessions.NewCookieStore([]byte("something-very-secret"))
var decoder = schema.NewDecoder()
var cron_process = cron.New()
var db*sql.DB
var cronId = make(map[string]cron.EntryID)
var errLog *log.Logger

// Types Personalis�s et methodes de types

type CronEntry struct {
    Id  int
    Cluster string
    Repository string 
    Name string 
    Seconds string 
    Minutes string 
    Hours string 
    DayOfMonth string 
    Month string 
    DayOfWeek string 
}

func (c CronEntry) cronSpecs() string {
    return c.Seconds + " " + c.Minutes + " " + c.Hours + " " + c.DayOfMonth + " " + c.Month + " " + c.DayOfWeek
}

type Configuration struct {
    NodeIP            string
    NodePort          string
    ListenPort        string
    SnapshotRetention int
}

type ClusterResponse struct {
    Name        string `json:"name"`
    ClusterName string `json:"cluster_name"`
    ClusterUuid string `json:"cluster_uuid"`
    Version     struct {
        Number         string `json:"number"`
        BuildHash      string `json:"build_hash"`
        BuildTimestamp string `json:"build_timestamp"`
        BuildSnapshot  bool   `json:"build_snapshot"`
        LuceneVersion  string `json:"lucene_version"`
    } `json:"version"`
    TagLine string `json:"tagline"`
}

type ClusterRepositoryResponse map[string]*ClusterRepositoryResponseMetaData

type ClusterRepositoryResponseMetaData struct {
    Type     string                 `json:"type"`
    Settings map[string]interface{} `json:"settings,omitempty"`
}

type RepositorySnapshotsResponse struct {
    Snapshots []*RepositorySnapshotsResponseMetaData     
}

type RepositorySnapshotsResponseMetaData struct {
    Snapshot     string                 `json:"snapshot"`
    VersionId    int                    `json:"version_id"`
    Version      string                 `json:"version"`
    Indices      []string               `json:"indices"`
    State        string                 `json:"state"`
    StartTime    time.Time              `json:"start_time"`
    StartTimeMs  int64                  `json:"start_time_in_millis"`
    EndTime      time.Time              `json:"end_time"`
    EndTimeMs    int64                  `json:"end_time_in_millis"`
    Duration     int64                  `json:"duration_in_millis"`
    Failures     []*SnapshotFailure     `json:"failures"`
    Shards       struct {
        Total          int           `json:"total"`
        Failed         int           `json:"failed"`
        Successful     int           `json:"successful"`
    } `json:"shards"`
}

type SnapshotFailure struct {
    Index     string `json:"index"`
    IndexUuid string `json:"index_uuid"`
    ShardId   int    `json:"shard_id"`
    Reason    string `json:"reason"`
    NodeId    string `json:"node_id"`
    Status    string `json:"status"`
}

// Fonctions utilitaires
func db_InitDB(filepath string) (*sql.DB, error) {
    db, err := sql.Open("sqlite3", filepath)
    if err != nil { 
        return db, err
    } 
    if db == nil {
        return db, err
    } 

    return db, err
}

func db_CreateTable(db *sql.DB) error {
    sql_table := `
    CREATE TABLE IF NOT EXISTS crons(
        Id INTEGER PRIMARY KEY AUTOINCREMENT,
        Cluster TEXT,
        Repository TEXT,
        Name TEXT,
        Seconds TEXT,
        Minutes TEXT,
        Hours TEXT,
        DayOfMonth TEXT,
        Month TEXT,
        DayOfWeek TEXT,
    CONSTRAINT name_unique UNIQUE (Name)
    );
    `

    _, err := db.Exec(sql_table)
    return err
}

func db_StoreCron(db *sql.DB, cron CronEntry) (int64, error) {
    sql_additem := `
    INSERT OR REPLACE INTO crons(   
        Cluster,   
        Repository,
        Name,
        Seconds,
        Minutes,
        Hours,
        DayOfMonth,
        Month,
        DayOfWeek
    ) values( ?, ?, ?, ?, ?, ?, ?, ?, ?)`

    var err error
    var id int64 = 0

    stmt, err := db.Prepare(sql_additem)
    if err != nil {
        return id, err
    }
    defer stmt.Close()

    

    res, err := stmt.Exec(cron.Cluster, cron.Repository, cron.Name, cron.Seconds, cron.Minutes, cron.Hours, cron.DayOfMonth, cron.Month, cron.DayOfWeek)
    if err != nil {
        return id, err
    }
    id, err = res.LastInsertId()
    if err != nil {
        return id, err
    }
    return id, err
}

func db_ReadCrons(db *sql.DB) ([]CronEntry, error) {
    sql_readall := `
    SELECT Id, Cluster, Repository, Name, Seconds, Minutes, Hours, DayOfMonth, Month, DayOfWeek FROM crons
    ORDER BY datetime(Id) ASC`

    var crons []CronEntry

    rows, err := db.Query(sql_readall)
    if err != nil { 
        return crons, err
    } 
    defer rows.Close()

    
    for rows.Next() {
        cron := CronEntry{}
        err = rows.Scan(&cron.Id, &cron.Cluster, &cron.Repository, &cron.Name, &cron.Seconds, &cron.Minutes, &cron.Hours, &cron.DayOfMonth, &cron.Month, &cron.DayOfWeek)
        if err != nil {
            return crons, err
        }
        crons = append(crons, cron)
    }
    return crons, err
}

func db_ReadCron(db *sql.DB, id int64) (CronEntry, error) {
    sql_readall := `
    SELECT Id, Cluster, Repository, Name, Seconds, Minutes, Hours, DayOfMonth, Month, DayOfWeek FROM crons
    WHERE Id=?`

    var cron CronEntry

    row, err := db.Prepare(sql_readall)
    if err != nil {
        return cron, err 
    }
    defer row.Close()

    
    err = row.QueryRow(id).Scan(&cron.Id, &cron.Cluster, &cron.Repository, &cron.Name, &cron.Seconds, &cron.Minutes, &cron.Hours, &cron.DayOfMonth, &cron.Month, &cron.DayOfWeek)
    if err != nil {
        return cron, err 
    }   
    
    return cron, err
}

func db_ReadRepositoryCrons(db *sql.DB, repository string) ([]CronEntry, error) {
    sql_readall := `
    SELECT Id, Cluster, Repository, Name, Seconds, Minutes, Hours, DayOfMonth, Month, DayOfWeek FROM crons
    WHERE Repository=?
    ORDER BY datetime(Id) ASC`

    var crons []CronEntry

    rows, err := db.Query(sql_readall, repository)
    if err != nil { 
        return crons, err
    } 
    defer rows.Close()

    
    for rows.Next() {
        cron := CronEntry{}
        err = rows.Scan(&cron.Id, &cron.Cluster, &cron.Repository, &cron.Name, &cron.Seconds, &cron.Minutes, &cron.Hours, &cron.DayOfMonth, &cron.Month, &cron.DayOfWeek)
        if err != nil {
            return crons, err
        }
        crons = append(crons, cron)
    }
    return crons, err
}

func db_DeleteCron(db *sql.DB, id int64) (int64, error) {
    sql_readall := `
    DELETE FROM crons
    WHERE Id=?`

    var affected int64 = 0

    row, err := db.Prepare(sql_readall)
    if err != nil { 
        return affected, err
    }
    defer row.Close()

    res, e := row.Exec(id)
    if e != nil { 
        return affected, e
    }

    affected, err = res.RowsAffected()
    if err != nil { 
        return affected, err
    }
    return affected, err
}

func respondError(r *http.Request, status int, w http.ResponseWriter, err error) {
	errLog.Println(err)
	errorTemplate := template.Must(template.New("error").Parse(errorTemplateString))
	templateContext := struct { Host string; Status int; StatusText string; Es_ip string; Es_port string; } { r.Host, status, http.StatusText(status), *es_ip_Ptr, *es_port_Ptr, }
	w.WriteHeader(status)
	err = errorTemplate.Execute(w, templateContext)
    if err != nil{
        errLog.Println(err)
        fmt.Fprintf(w,"<!doctype html><html><body><h1>Oups! Irrecoverable internal error! <br>Please open an issue on <a href=\"https://bitbucket.org/MathayJeanLuc/els-snapshot-manager/issues?status=new&status=open\">Bitbucket project page</a></h3></body></html>")
    }
}

func GetClusterResponse() (*ClusterResponse, int, error) {
    var ret = new(ClusterResponse)
    response, err := http.Get("http://" + *es_ip_Ptr + ":" + *es_port_Ptr)
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return ret, 500, err
    }

    if response.StatusCode != 200 {
        return ret, response.StatusCode, err
    } 

    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        return ret, 500, err
    }

    err = json.Unmarshal(body, &ret)
    if(err != nil){
        return ret, 500, err
    }

    return ret, response.StatusCode, err
}

func GetClusterRepositoryResponse() (*ClusterRepositoryResponse, int, error) {
    var ret = new(ClusterRepositoryResponse)
    response, err := http.Get("http://" + *es_ip_Ptr + ":" + *es_port_Ptr + "/_snapshot")
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return ret, 500, err
    }

    if response.StatusCode != 200 {
        return ret, response.StatusCode, errors.New("Cluster response != 200")
    } 

    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        return ret, 500, err
    }
    
    err = json.Unmarshal(body, &ret)
    if(err != nil){
        return ret, 500, err
    }

    return ret, response.StatusCode, err
}

func GetRepositorySnapshotsResponse(repository string ) (*RepositorySnapshotsResponse, int, error) {
    var ret = new(RepositorySnapshotsResponse)
    response, err := http.Get("http://" + *es_ip_Ptr + ":" + *es_port_Ptr + "/_snapshot/" + repository + "/_all")
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return ret, 500, err
    }   

    if response.StatusCode != 200 {
        return ret, response.StatusCode, errors.New("Cluster response != 200")
    }  

    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        return ret, 500, err
    }
    
    err = json.Unmarshal(body, &ret)
    if(err != nil){
        return ret, 500, err
    } 
    return ret, response.StatusCode, err   
}

func GetRepositorySnapshotResponse(repository string, snaphot string ) (*RepositorySnapshotsResponse, int, error) {
    var ret = new(RepositorySnapshotsResponse)
    response, err := http.Get("http://" + *es_ip_Ptr + ":" + *es_port_Ptr + "/_snapshot/" + repository + "/" + snaphot)
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return ret, 500, err
    }    

    if response.StatusCode != 200 {
        return ret, response.StatusCode, errors.New("Cluster response != 200")
    } 

    body, err := ioutil.ReadAll(response.Body)
    if err != nil {
        return ret, 500, err
    }

    err = json.Unmarshal(body, &ret)
    if(err != nil){
        return ret, 500, err
    } 
        
    return ret, response.StatusCode, err    
}

func DoCreateSnapshot(cluster string, repository string,) (error){
    PurgeSnapshots(repository, *snapshot_retention_ptr)

    client := &http.Client{}
    t := time.Now()
    snapshot := strings.ToLower(cluster) + "_" + t.Format("20060102-150405")     

    request, err := http.NewRequest("PUT", "http://" + *es_ip_Ptr + ":" + *es_port_Ptr + "/_snapshot/" + repository + "/" + snapshot + "?wait_for_completion=false", nil)
    if err != nil {
        return err
    }
    request.Close = true

    response, err := client.Do(request)
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return err
    }
    client = nil
    return err
}

func DoDeleteSnapshot(repository string, snapshot string) (error){ 
    client := &http.Client{}
    request, err := http.NewRequest("DELETE", "http://" + *es_ip_Ptr + ":" + *es_port_Ptr + "/_snapshot/" + repository + "/" + snapshot, nil)
    if err != nil {
        return err
    }
    request.Close = true

    response, err := client.Do(request)
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return err
    }
    client = nil
    return err
}

func DoRestoreSnapshot( repository string, snapshot string) (int, error){
    client := &http.Client{}

    request, err := http.NewRequest("POST", "http://" + *es_ip_Ptr + ":" + *es_port_Ptr + "/_snapshot/" + repository + "/" + snapshot + "/_restore", nil)
    if err != nil {
        return 500, err
    }
    request.Close = true

    response, err := client.Do(request)
    
    if response != nil {
        defer response.Body.Close()
    }
    if err != nil {
        return 500, err
    }
    client = nil
    return response.StatusCode, err
}

func PurgeSnapshots(repository string, toKeep int) error {
    repositorySnapshotsResponse, status, err := GetRepositorySnapshotsResponse(repository)
    if status != 200 {
        return errors.New("Cluster response != 200")
    }

    if len(repositorySnapshotsResponse.Snapshots) > toKeep {

        toDelete := repositorySnapshotsResponse.Snapshots[0:len(repositorySnapshotsResponse.Snapshots)-toKeep+1]

        for key, value := range toDelete {
            DoDeleteSnapshot(repository, value.Snapshot)
        _ = key
        }
    }
    return err
}

func LoadConfiguration(file string) (*Configuration, error) {
    var config = new(Configuration)

    configFile, err := os.Open(file)
    defer configFile.Close()
    if err != nil {
        return config, err
    }

    jsonParser := json.NewDecoder(configFile)
    err = jsonParser.Decode(&config)
    if err != nil {
        return config, err
    }

    return config, err
}

// Fonctions handlers
func GetCluster(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    clusterResponse, status, err := GetClusterResponse()
    if status != 200 {
        respondError(r, status, w, err)
        return
    }

    repositoryResponse, status, err := GetClusterRepositoryResponse()
    if status != 200 {
        respondError(r, status, w, err)
        return
    }

    session.Values["cluster"] = clusterResponse.ClusterName
    sessionError := session.Save(r, w) 
    if sessionError != nil {
        respondError(r, 500, w, err)
        return
    }
    rootTemplate := template.Must(template.New("root").Parse(clusterTemplateString))
    templateContext := struct { Cluster *ClusterResponse; Repositories *ClusterRepositoryResponse; Host string } { clusterResponse, repositoryResponse, r.Host }
    err = rootTemplate.Execute(w, templateContext)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
}

func GetRepository(w http.ResponseWriter, r *http.Request) {    
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]

    if session.Values["cluster"] != cluster {
    	if session.Values["cluster"] == nil {
    		http.Redirect(w, r, "http://" + r.Host, 302)
    		return
    	}
    	respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
			return
    }

    repositorySnapshotsResponse, status, err := GetRepositorySnapshotsResponse(repository)
    if status != 200 {
			respondError(r, status, w, err)
			return
    }
    repositoryTemplate := template.Must(template.New("repository").Parse(repositoryTemplateString))
    templateContext := struct { Host string; Cluster string; Repository string; RepositorySnapshots *RepositorySnapshotsResponse; } { r.Host, cluster, repository, repositorySnapshotsResponse }
    err = repositoryTemplate.Execute(w, templateContext)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
}

func CreateSnapshot(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]

    if session.Values["cluster"] != cluster {
        if session.Values["cluster"] == nil {
        	http.Redirect(w, r, "http://" + r.Host, 302)
        	return
        }
        respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
        return
    } 

    err = DoCreateSnapshot(cluster, repository)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
    http.Redirect(w, r, "http://" + r.Host + "/" + cluster + "/" + repository, 302)
}

func GetSnapshot(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]
    snapshot := params["snapshotName"]

    if session.Values["cluster"] != cluster {
    		if session.Values["cluster"] == nil {
    		http.Redirect(w, r, "http://" + r.Host, 302)
    		return
    	}
    	respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
      return
    }

    snapshotResponse, status, err := GetRepositorySnapshotResponse(repository, snapshot)
    if status != 200 {
      respondError(r, status, w, err)
      return
    }
    snapshotTemplate := template.Must(template.New("repository").Parse(snapshotTemplateString))
    templateContext := struct { Host string; Cluster string; Repository string; Snapshot string; Snapshots *RepositorySnapshotsResponse; } { r.Host, cluster, repository, snapshot, snapshotResponse }
    err = snapshotTemplate.Execute(w, templateContext)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
}

func PostSnapshot(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]
    snapshot := params["snapshotName"]

    if session.Values["cluster"] != cluster {
    	if session.Values["cluster"] == nil {
    		http.Redirect(w, r, "http://" + r.Host, 302)
    		return
    	}
    	respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
      return
    }

    method := r.FormValue("_method")

    if method == "delete" {
	    err = DoDeleteSnapshot(repository, snapshot)
	    if err != nil {
	    		errLog.Println(err)
	        respondError(r, 500, w, err)
	        return
	    }
  	} else if method == "restore" {
  		status, err := DoRestoreSnapshot(repository, snapshot)
	    if err != nil {
	    		errLog.Println(err)
	        respondError(r, status, w, err)
	        return
	    }
	    if status != 200 {
	    	errLog.Println(err)
        respondError(r, status, w, err)
        return
	    }
  	} else {
  		respondError(r, 500, w, errors.New("invalid action"))
  		return
  	}
    
    http.Redirect(w, r, "http://" + r.Host + "/" + cluster + "/" + repository, 302)    
}

func GetCron(w http.ResponseWriter, r *http.Request) {    
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]

    if session.Values["cluster"] != cluster {
            if session.Values["cluster"] == nil {
            http.Redirect(w, r, "http://" + r.Host, 302)
            return
        }
        respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
      return
    }

    repositoryResponse, status, err := GetClusterRepositoryResponse() 
    if status != 200 {
        respondError(r, 404, w, err)
        return
    }
    if _, ok := (*repositoryResponse)[repository]; !ok {
        respondError(r, 404, w, err)
        return
    }    

    allCrons, err := db_ReadRepositoryCrons(db, repository) 
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    var crons []CronEntry

    for key, value := range allCrons {
        if value.Repository == repository {
            crons = append(crons, value)
        }
        _ = key
    }

    cronTemplate := template.Must(template.New("getCron").Parse(getCronTemplateString))
    templateContext := struct { Host string; Cluster string; Repository string; Crons []CronEntry } { r.Host, cluster, repository, crons }
    err = cronTemplate.Execute(w, templateContext)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
}

func NewCron(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]

    if session.Values["cluster"] != cluster {
            if session.Values["cluster"] == nil {
            http.Redirect(w, r, "http://" + r.Host, 302)
            return
        }
        respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
      return
    }

    repositoryResponse, status, err := GetClusterRepositoryResponse() 
    if status != 200 {
        respondError(r, 404, w, err)
        return
    }
    if _, ok := (*repositoryResponse)[repository]; !ok {
        respondError(r, 404, w, err)
        return
    }
    var newCron CronEntry
    cronTemplate := template.Must(template.New("newCron").Parse(newCronTemplateString))
    templateContext := struct { Host string; Cluster string; Repository string; Cron CronEntry; Msg string} { r.Host, cluster, repository, newCron, "" }
    err = cronTemplate.Execute(w, templateContext)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
}

func SetCron(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
    		respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]

    if session.Values["cluster"] != cluster {
            if session.Values["cluster"] == nil {
            http.Redirect(w, r, "http://" + r.Host, 302)
            return
        }
        respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
      return
    }

    repositoryResponse, status, err := GetClusterRepositoryResponse() 
    if status != 200 {
        respondError(r, 404, w, err)
        return
    }
    if _, ok := (*repositoryResponse)[repository]; !ok {
        respondError(r, 404, w, err)
        return
    }

    err = r.ParseForm()
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    
    var newcron CronEntry

    err = decoder.Decode(&newcron, r.PostForm)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    newcron.Cluster = cluster
    newcron.Repository = repository

    var id int64 = 0
    id, err = db_StoreCron(db, newcron) 
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    for k, v := range cronId {
        if k == newcron.Name { 
            delete(cronId, k)
            cron_process.Remove(v)
        }
    }

    newCronId, cronError := cron_process.AddFunc(newcron.cronSpecs(), func() {DoCreateSnapshot(cluster, repository)} )

    if cronError !=nil {
        _, e := db_DeleteCron(db, id)
        if e != nil {
            respondError(r, 500, w, err)
            return
        }
        cronTemplate := template.Must(template.New("newCron").Parse(newCronTemplateString))
        templateContext := struct { Host string; Cluster string; Repository string; Cron CronEntry; Msg string } { r.Host, cluster, repository, newcron, "Error while parsing your cron values!" }
        err = cronTemplate.Execute(w, templateContext)
        return
        if err != nil {
            respondError(r, 500, w, err)
        return
        }
    }

    cronId[newcron.Name] = newCronId

    http.Redirect(w, r, "http://" + r.Host + "/" + cluster + "/" + repository + "/cron", 302)
}

func DeleteCron(w http.ResponseWriter, r *http.Request) {
    session, err := store.Get(r, "ELS_SnaphotManager")
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    params := mux.Vars(r)
    cluster := params["clusterName"]
    repository := params["repositoryName"]
    str_id := params["id"]

    if session.Values["cluster"] != cluster {
            if session.Values["cluster"] == nil {
            http.Redirect(w, r, "http://" + r.Host, 302)
            return
        }
        respondError(r, 404, w, errors.New("Bad URL: cluster not the same as session value"))
      return
    }

    repositoryResponse, status, err := GetClusterRepositoryResponse() 
    if status != 200 {
        respondError(r, 404, w, err)
        return
    }
    if _, ok := (*repositoryResponse)[repository]; !ok {
        respondError(r, 404, w, err)
        return
    }

    id, err := strconv.ParseInt(str_id, 10, 64)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    cron, err := db_ReadCron(db,id)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }

    res, err := db_DeleteCron(db, id)
    if err != nil {
        respondError(r, 500, w, err)
        return
    }
    _ = res

    for k, v := range cronId {
        if k == cron.Name { 
            delete(cronId, k)
            cron_process.Remove(v)
        }
    }

    http.Redirect(w, r, "http://" + r.Host + "/" + cluster + "/" + repository + "/cron", 302)
}


// Fonctions initialisation & main
func init() {
    error_counter := 0

    configuration, err := LoadConfiguration("./config/config.json")
    if err != nil {
        fmt.Printf("Unable to parse configuration file at ./config/config.json\n")
        fmt.Printf(err.Error()+ "\n")
        os.Exit(1)
    }
    
    es_ip_Ptr = &configuration.NodeIP
    es_port_Ptr = &configuration.NodePort
    listen_port_Ptr =&configuration.ListenPort
    snapshot_retention_ptr = &configuration.SnapshotRetention

    if net.ParseIP(*es_ip_Ptr) == nil {
        fmt.Printf("%v is not a valid IP address\n", *es_ip_Ptr)
        error_counter ++
    }

    _, err = strconv.ParseUint(*es_port_Ptr, 10, 16)
    if err != nil {
        fmt.Printf("%v is not a valid port number\n", *es_port_Ptr)
        error_counter ++
    }

    _, err = strconv.ParseUint(*listen_port_Ptr, 10, 16)
    if err != nil {
        fmt.Printf("%v is not a valid port number\n", *listen_port_Ptr)
        error_counter ++
    }

    if error_counter != 0 {
        os.Exit(1)
    }

    db, err = db_InitDB(dbpath)
    if err != nil {
        fmt.Printf("Unable to init database")
        os.Exit(1)
    }

    err = db_CreateTable(db)
    if err != nil {
        fmt.Printf("Unable to create crons table in database")
        os.Exit(1)
    }
}

func main() {

		e, err := os.OpenFile("./log/ELS_SnapshotManager.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)

    if err != nil {
            fmt.Printf("error opening log file: %v", err)
            os.Exit(1)
    }
    errLog = log.New(e, "", log.Ldate|log.Ltime)
    errLog.SetOutput(&lumberjack.Logger{
      Filename:   "./log/ELS_SnapshotManager.log",
      MaxSize:    10, // megabytes after which new file is created
      MaxAge:     30, //days
    })

    crons, err := db_ReadCrons(db)
    if err != nil {
    		errLog.Println(err)
        fmt.Printf("Unable to retrieve crons from database")
        os.Exit(1)
    }

    for _, c := range crons {
        id, err := cron_process.AddFunc(c.cronSpecs(), func() { DoCreateSnapshot(c.Cluster, c.Repository) })
        if err !=nil {
        		errLog.Println(err)
            fmt.Printf("Error while setting crons")
            os.Exit(1)
        }
        cronId[c.Name] = id
    }

    cron_process.Start()    
    
    router := mux.NewRouter()
    router.HandleFunc("/", GetCluster).Methods("GET")    

    router.HandleFunc("/{clusterName}/{repositoryName}", GetRepository).Methods("GET") 
    router.HandleFunc("/{clusterName}/{repositoryName}", CreateSnapshot).Methods("POST") 
    
    router.HandleFunc("/{clusterName}/{repositoryName}/cron", GetCron).Methods("GET")
    router.HandleFunc("/{clusterName}/{repositoryName}/cron/new", NewCron).Methods("GET")
    router.HandleFunc("/{clusterName}/{repositoryName}/cron/new", SetCron).Methods("POST")
    router.HandleFunc("/{clusterName}/{repositoryName}/cron/{id:[0-9]+}", DeleteCron).Methods("POST")

    router.HandleFunc("/{clusterName}/{repositoryName}/{snapshotName}", GetSnapshot).Methods("GET") 
    router.HandleFunc("/{clusterName}/{repositoryName}/{snapshotName}", PostSnapshot).Methods("POST")     

    errLog.Fatal(http.ListenAndServe(":" + *listen_port_Ptr, router))
}