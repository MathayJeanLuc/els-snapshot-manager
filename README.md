# ELS Snapshot manager

A simple web interface to manage Elasticsearch cluster snapshots...


###1°) Build with a correctly configured Go toolchain :
	* go get -u github.com/gorilla/mux
	* clone this repos
	* go build ELS_SnapshotManager.go

Source contains compiled executable (go1.10.2 linux/amd64) if you don't want to build the project.

###2°)  Run it:
Be sure to have a config and log folders in ELS_SnapshotManager folder:

{

	"nodeIP":"127.0.0.1",

	"nodePort":"9200",

	"listenPort":"8090",

	"snapshotRetention": 30
	
}

config folder must contain a valid config.json file


####Example : 
	* ./ELS_SnapshotManager

You can now inspect the cluster registered repositories, list all snapshots, delete them or create new...

You can also automate snapshot creation for each repository

If you have several ES nodes it's better to put a copy of ELS_SnapshotManager on each node



###TODOS :
	* Add support for external templates files in ./templates
	* Add restore snapshot functionality

